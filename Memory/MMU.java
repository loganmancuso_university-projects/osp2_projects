/****************************************************************
 * 'MMU.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:57:12
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Memory;
import java.util.*;
import osp.IFLModules.*;
import osp.Threads.*;
import osp.Tasks.*;
import osp.Utilities.*;
import osp.Hardware.*;
import osp.Interrupts.*;

/****************************************************************
 * Class MMU.java
**/

public class MMU extends IflMMU {

  /****************************************************************
   * Constructor
  **/

  /****************************************************************
   * Functions
   * 'init'
   * 'do_refer'
   * 'atError'
   * 'atWarning'
  **/
  public static void init() {
    for( int i = 0; i < MMU.getFrameTableSize(); i++ ) {
      setFrame( i, new FrameTableEntry( i ) );
    }//end for i
  }//end init

  static public PageTableEntry do_refer( int memory_address, int reference_type, ThreadCB thread ) {
    int page_number = memory_address / ( int )Math.pow( 2.0, getVirtualAddressBits() - getPageAddressBits() );
    PageTableEntry temp_page_table_entry = getPTBR().pages[page_number];
    if( temp_page_table_entry.isValid() ) {
      temp_page_table_entry.getFrame().setReferenced( true );
      if( reference_type == GlobalVariables.MemoryWrite ) {
        temp_page_table_entry.getFrame().setDirty( true );
      }//end if
      return temp_page_table_entry;
    }//end if
    else if( temp_page_table_entry.getValidatingThread() == null ) {
      temp_page_table_entry.page_faulted = true;
      InterruptVector.setInterruptType( reference_type );
      InterruptVector.setPage( temp_page_table_entry );
      InterruptVector.setThread( thread );
      CPU.interrupt( PageFault );
      if( thread.getStatus() == GlobalVariables.ThreadKill ) {
        return temp_page_table_entry;
      }//end if
    }//end else if
    else {
      thread.suspend( temp_page_table_entry );
      if( thread.getStatus() == GlobalVariables.ThreadKill ) {
        return temp_page_table_entry;
      }//end if
    }//end else
    temp_page_table_entry.getFrame().setReferenced( true );
    if( reference_type == GlobalVariables.MemoryWrite ) {
      temp_page_table_entry.getFrame().setDirty( true );
    }//end if
    return temp_page_table_entry;
  }//end do_refer

  public static void atError() {
    System.out.println( "An Error Has Occurred" );
  }//end atError

  public static void atWarning() {
    System.out.println( "A Warning Has Occurred" );
  }//end atWarning

}//end class MMU

/****************************************************************
 * End 'MMU.java'
**/
