/****************************************************************
 * 'ThreadCB.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 09-14-2017--21:58:29
 * Room: 3A11
 * Sources: gitHub and text book
 * including source code and documentation provided by
 * the text book for understanding and implementing methods
 * and for creating and setting functions. web resources used
 * were for reference to uses pertaining to the Generic List and
 * the functions associated with it and the creation of threads
 * in a queued system.
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Threads;
import java.util.Vector;
import java.util.Enumeration;
import osp.Utilities.*;
import osp.IFLModules.*;
import osp.Tasks.*;
import osp.EventEngine.*;
import osp.Hardware.*;
import osp.Devices.*;
import osp.Memory.*;
import osp.Resources.*;

/****************************************************************
 * This class is responsible for actions related to a thread,
 * including: creating, killing, dispatching, resuming, and
 * suspending threads.
**/

public class ThreadCB extends IflThreadCB {

  /****************************************************************
   * Variables
  **/
  static GenericList queue_; //queue for threads

  /****************************************************************
   * Function 'ThreadCB'
   * function calls parent class to implement functions
  **/
  public ThreadCB() {
    super(); //calls parent class
  }//end ThreadCB

  /****************************************************************
   * Function 'init'
   * initialize the ready queue for the threads using a built-in
   * generic queue
  **/
  public static void init() {
    queue_ = new GenericList(); //initialize
  }//end init

  /****************************************************************
   * Function 'do_create'
   * this function will create a thread given some task, from this
   * it will check the thread count and determine if there needs
   * to be a new thread dispatched. If it cannot add it will
   * dispatch a new thread and run the task
  **/
  static public ThreadCB do_create(TaskCB task) {
    if( task == null ) {
    //  System.out.println("task null");
      dispatch();
      return null;
    }//end if
    if( task.getThreadCount() >= MaxThreadsPerTask ) {
    //  System.out.println("task full");
      dispatch();
      return null;
    }
    ThreadCB thread_ = new ThreadCB(); //call new thread
    thread_.setPriority(task.getPriority());
    thread_.setStatus(ThreadReady);
    thread_.setTask(task);
 //   try {
      if( task.addThread(thread_) == 0 ) { //if cannot place
        // System.out.println("cannot add");
        dispatch();
        return null;
      }//end if
  //  }//end try
  //  catch( NullPointerException exception ) {
  //  } //end catch
    queue_.append(thread_); //add to end of queue
    dispatch();
    return thread_;
  }//end do_create

  /****************************************************************
   * Function 'do_kill'
   * This function is responsible for removing a thread from the
   * queue and from killing the thread from running once complete.
   * It will determine the running state from the status method in
   * the thread class.
  **/
  public void do_kill() {
    ThreadCB a_task = null; //this.getTask();
    switch( this.getStatus() ) {
      case ThreadReady: //if a thread is in the queue
        // System.out.println("thread ready status");
        if( queue_.contains(this) ) {
          queue_.remove(this);
        }//end if
        break;
      case ThreadRunning: //if a thread is currently running
        // System.out.println("thread running status");
        try {
          a_task = MMU.getPTBR().getTask().getCurrentThread();
          if( a_task == this ) {
            MMU.setPTBR(null);
            getTask().setCurrentThread(null);
          }//end if
        }//end try
        catch( NullPointerException exception ) {
        }//end catch
      default:
        break;
    }//end switch case
    this.getTask().removeThread(this);
    this.setStatus(ThreadKill);
    for( int i = 0; i < Device.getTableSize(); i++ ) {
      Device.get(i).cancelPendingIO(this);
    }//end for
    ResourceCB.giveupResources(this);
    if( this.getTask().getThreadCount() == 0 ) {
      getTask().kill();
      // System.out.println("threads complete killing task");
    }//end if
    dispatch();
  }//end do_kill

  /****************************************************************
   * Function 'do_suspends'
   * this function will stop the thread if it is running. I will
   * evaluate the waiting status and if it is in the queue.
  **/
  public void do_suspend(Event event) {
    if( (this.getStatus() == ThreadRunning) )
    {
      try {
        ThreadCB thread_ = MMU.getPTBR().getTask().getCurrentThread();
        if( this == thread_ ) {
          MMU.setPTBR(null);
          this.getTask().setCurrentThread(null);
          setStatus(ThreadWaiting);
          // System.out.println("thread set to waiting status");
        }//end if
      }//end try
      catch( NullPointerException exception ) {
      }//end catch
    }//end if
    //is a waiting thread
    else if( this.getStatus() >= ThreadWaiting ) {
      this.setStatus(this.getStatus() + 1);
      // System.out.println("set status + 1 " + getStatus());
      //queue does not contain thread so add to event
    }//end if
    if( !queue_.contains(this) ) {
      event.addThread(this);
      // System.out.println("not in queue - added thread");
    }//end if
    dispatch();
  }//end do_suspends

  /****************************************************************
   * Function 'do_resume'
   * this function will resume the thread after comparing the
   * thread waiting is called. It pulls the value of the thread
   * and will change the status if it had run
  **/
  public void do_resume() {
    if( this.getStatus() == ThreadWaiting ) {
      setStatus(ThreadReady);
      //if( getStatus() == ThreadReady ) {
        queue_.append(this);
        // System.out.println("added thread back to queue");
      //}//end if
    }//end if
    else {
      setStatus(getStatus() - 1);
      // System.out.println("set status - 1 " + getStatus());
    }//end else if
    dispatch();
  }//end do_resume

  /****************************************************************
   * Function 'do_dispatch'
   * this function will dispatch a thread and let it run passing
   * the values of the task to the thread and running. It will
   * stop the CPU to check the values and add to the queue
  **/
  public static int do_dispatch() {
    ThreadCB thread_ = null;
    try {
      thread_ = MMU.getPTBR().getTask().getCurrentThread();
    }//end try
    catch( NullPointerException exception ) {
    }//end catch
    if( thread_ != null ) {
      thread_.getTask().setCurrentThread(null);
      MMU.setPTBR(null);
      thread_.setStatus(ThreadReady);
      queue_.append(thread_);
    }//end if
    if( queue_.isEmpty() ) {
      MMU.setPTBR(null);
      // System.out.println("queue is empty, failed");
      return FAILURE;
    }//end if
    else {
      ThreadCB new_thread_ = (ThreadCB)queue_.removeHead();
      MMU.setPTBR(new_thread_.getTask().getPageTable());
      new_thread_.getTask().setCurrentThread(new_thread_);
      new_thread_.setStatus(ThreadRunning);
      //return SUCCESS;
    }//end else
    return SUCCESS;
  }//end do_dispatch

  /****************************************************************
   * Function 'atError'
  **/
  public static void atError() {
    // System.out.println("An Error Has Occurred");
  }//end atError

  /****************************************************************
   * Function 'atWarning'
  **/
  public static void atWarning() {
    // System.out.println("A Warning For The Program");
  }//end atWarning

}//end class ThreadCB

/****************************************************************
 * End 'ThreadCB.java'
**/
