#!/bin/bash

###
# 'compile.sh'
# this program will compile the program and output errors to
# a text file called xout.txt
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--08:57:12
###

# compile program
# ./zaRefreshDate.sh
 javac -g -classpath .:OSP.jar: -d . *.java |& tee xout.txt

###
# End 'compile.sh'
###