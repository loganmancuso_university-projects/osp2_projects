/****************************************************************
 * 'TimerInterruptHandler.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 09-14-2017--21:58:38
 *
**/


/****************************************************************
 * Imports and Packages
**/
package osp.Threads;
import osp.IFLModules.*;
import osp.Utilities.*;
import osp.Hardware.*;

/****************************************************************
 * This is the timer interrupt handler and will be called to 
 * handle the timer interrupts. For FCFS scheduling, this class 
 * will sit and call for a thread dispatch because no time quantum
 * is used.
**/

public class TimerInterruptHandler extends IflTimerInterruptHandler {
	public void do_handleInterrupt() {
    ThreadCB.dispatch(); //dispatch a new thread
	}//end class do_handleInterrupt

}//end class TimerInterruptHandler


/****************************************************************
 * End 'TimerInterruptHandler.java'
**/
