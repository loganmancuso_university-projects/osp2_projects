#!/bin/bash

###
# 'run.bat'
# this program will run the simulation from command prompt
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--08:57:12
###

java -classpath .:OSP.jar osp.OSP | tee x.out

###
# End 'run.bat'
###