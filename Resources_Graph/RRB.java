/****************************************************************
 * 'RRB.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-01-2017--07:18:21
 *
**/

/****************************************************************
 * Imports and Packages
**/

Repackage osp.Resources;
import java.util.*;
import osp.Utilities.*;
import osp.IFLModules.*;
import osp.Threads.*;

/****************************************************************
 *
**/

public class RRB extends IflRRB {

  /****************************************************************
   * Constructor 'RRB'
   * call parent class and pass parameters
   * 'This is the class constructor. The first statement in this
   * constructor must be super(thread, resource, quantity), but
   * the rest depends on your program design.'
  **/
  public RRB(ThreadCB thread, ResourceCB resource,int quantity) {
    super( thread, resource, quantity);
  }//end RRB

  /****************************************************************
   * Function 'do_grant'
  **/
  public void do_grant() {
    ThreadCB a_thread = this.getThread();
    ResourceCB a_resource = this.getResource();
    a_resource.setAvailable( a_resource.getAvailable()
                              - this.getQuantity() );
    a_resource.setAllocated( a_thread,
                              a_resource.getAllocated( a_thread )
                              + this.getQuantity() );
    this.setStatus( GlobalVariables.Granted );
    this.notifyThreads();
  }//end do_grant

}//end RRB

/****************************************************************
 * End 'RRB.java'
**/
