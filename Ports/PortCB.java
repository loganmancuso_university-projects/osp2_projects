/****************************************************************
 * 'PortCB.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-29-2017--08:48:49
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Ports;
import java.util.*;
import osp.IFLModules.*;
import osp.Threads.*;
import osp.Tasks.*;
import osp.Memory.*;
import osp.Utilities.*;

/****************************************************************
 * Class 'PortCB'
**/
public class PortCB extends IflPortCB {

  /****************************************************************
   * Variables
   * Int
   * buffer_in
   * buffer_out
  **/
  private int buffer_in, buffer_out;

  /****************************************************************
  * Constructor
  **/
  public PortCB() {
    super(); //call parent class
  }//end PortCB

  /****************************************************************
   * Functions
   * 'init'
   * 'do_create'
   * 'do_destroy'
   * 'do_send'
   * 'do_receive'
   * 'atError'
   * 'atWarning'
  **/

  public static void init() {
    //buffer_in = buffer_out = 0;
  }//end init

  public static PortCB do_create() {
    PortCB a_port = new PortCB();
    TaskCB current_task = null;
    try {
      current_task = MMU.getPTBR().getTask();
    }//end try
    catch ( NullPointerException exception ) {
      System.out.println( exception.getMessage() );
    }//end catch
    int port_count = current_task.getPortCount();
    if( ( port_count == MaxPortsPerTask )
     || ( current_task.addPort( a_port ) == FAILURE ) )
    {
      return null;
    }//end if
    a_port.setTask( current_task );
    a_port.setStatus( PortLive );
    a_port.buffer_in = 0;
    a_port.buffer_out = 0;
    return a_port;
  }//end do_create

  public void do_destroy() {
    this.setStatus( PortDestroyed );
    this.notifyThreads();
    this.getTask().removePort( this );
    this.setTask( null );
  }//end do_destroy

  public int do_send( Message a_message ) {
    if( ( a_message == null )
     || ( a_message.getLength() > PortBufferLength ) )
    {
      return FAILURE;
    }//end if
    SystemEvent event = new SystemEvent("message suspended");
    ThreadCB a_thread = null;
    try {
      a_thread = MMU.getPTBR().getTask().getCurrentThread();
    }//end try
    catch( NullPointerException exception ) {
      System.out.println( exception.getMessage() );
    }//end catch
    a_thread.suspend( event );
    int buffer;
    boolean to_suspend = true;
    while( to_suspend ) {
      if( this.buffer_in < this.buffer_out ) {
        buffer = this.buffer_out - this.buffer_in;
      }//end if
      else if( this.buffer_in == this.buffer_out ) {
        if( this.isEmpty() ) {
          buffer = PortBufferLength;
        }//end if
        else {
          buffer = 0;
        }//end else
      }//end else if
      else {
        buffer = PortBufferLength + ( this.buffer_out - this.buffer_in );
      }//end else
      if( a_message.getLength() <= buffer ) {
        to_suspend = false;
      }//end if
      else {
        a_thread.suspend( this );
      }//end else
      if( a_thread.getStatus() == ThreadKill ) {
        this.removeThread( a_thread );
        return FAILURE;
      }//end if
      if( this.getStatus() != PortLive ) {
        event.notifyThreads();
        return FAILURE;
      }//end if
    }//end while
    this.appendMessage( a_message );
    this.notifyThreads();
    this.buffer_in = ( this.buffer_in + a_message.getLength() ) % PortBufferLength;
    event.notifyThreads();
    return SUCCESS; 
  }//end do_send

  public Message do_receive() {
    TaskCB a_task = null;
    ThreadCB a_thread = null;
    try {
      a_task = MMU.getPTBR().getTask();
      a_thread = a_task.getCurrentThread();
    }//end try
    catch( NullPointerException exception ) {
      System.out.println( exception.getMessage() );
    }//end catch
    if( this.getTask() != a_task ) {
      return null;
    }//end if
    SystemEvent event = new SystemEvent( "Receive Message" );
    a_thread.suspend( event );
    boolean to_suspend = true;
    while( to_suspend ) {
      if( this.isEmpty() ) {
        a_thread.suspend( this );
      }//end if
      else {
        to_suspend = false;
      }//end else
      if( a_thread.getStatus() == ThreadKill ) {
        this.removeThread( a_thread );
        event.notifyThreads();
        return null;
      }//end if
      if( this.getStatus() != PortLive ) {
        event.notifyThreads();
        return null;
      }//end if
    }//end while
    Message a_message = this.removeMessage();
    this.buffer_out = ( this.buffer_out + a_message.getLength() ) % PortBufferLength;
    this.notifyThreads();
    event.notifyThreads();
    return a_message;
  }//end do_receive

  public static void atError() {
    System.out.println( "An Error Has Occured" );
  }//end atError

  public static void atWarning() {
    System.out.println( "A Warning Has Occurred" );
  }//end atWarning



}//end class PortCB


/****************************************************************
 * End 'PortCB.java'
**/
