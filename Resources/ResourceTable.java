/****************************************************************
 * 'ResourceTable.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-01-2017--07:18:21
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Resources;
import java.util.*;
import osp.Utilities.*;
import osp.IFLModules.*;
import osp.Threads.*;
import osp.Tasks.*;

/****************************************************************
 * Class 'ResourceTable'
 * 'this is by far the easiest class. All you need to do is
 * implement the constructor. For the purposes of this project,
 * all you need do is call super().'
**/

public class ResourceTable extends IflResourceTable {

  /****************************************************************
   * Function 'ResourceTable'
   * calls parent class to implement functions
  **/
  public ResourceTable() {
    super();
  }//end ResourceTable

}//end ResourceTable

/****************************************************************
 * End 'ResourceTable.java'
**/
