/****************************************************************
 * 'ResourceCB.java'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-01-2017--07:18:21
 *
**/

/****************************************************************
 * Imports and Packages
**/
package osp.Resources;

import java.util.*;
import osp.IFLModules.*;
import osp.Tasks.*;
import osp.Threads.*;
import osp.Utilities.*;
import osp.Memory.*;

/****************************************************************
 * Class 'ResourceCB'
**/

public class ResourceCB extends IflResourceCB {
  /****************************************************************
   * Global Variables
  **/
  private static int size_;
  private static ArrayList<ThreadCB> thread_array_list_;
  private static ArrayList<RRB> rrb_array_list_;

  /****************************************************************
   * Constructor
  **/
  public ResourceCB(int qty) {
    super(qty);
  }//end ResourceCB

  /****************************************************************
   * Function 'init'
  **/
  public static void init() {
    size_ = ResourceTable.getSize();
    ResourceCB.thread_array_list_ = new ArrayList<ThreadCB>();
    ResourceCB.rrb_array_list_ = new ArrayList<RRB>();
  }//end init

  /****************************************************************
   * Function 'do_aquire'
  **/
  public RRB do_acquire(int quantity) {
    ThreadCB a_thread = null;
    try {
      a_thread = MMU.getPTBR().getTask().getCurrentThread();
    } //end try
    catch (NullPointerException exception) {
      System.out.println(exception.getMessage());
    } //end catch
    if ((this.getAvailable() < quantity) || (quantity < 0)
        || (quantity + this.getAllocated(a_thread) > this.getMaxClaim(a_thread))) {
      return null;
    } //end if
    ResourceCB.thread_array_list_.add(a_thread);
    RRB a_request = new RRB(a_thread, this, quantity);
    switch (IflResourceCB.getDeadlockMethod()) {
    case GlobalVariables.Avoidance:
      if (ResourceCB.bankers_algorithm()) {
        a_request.setStatus(GlobalVariables.Granted);
        a_request.grant();
      } //end if
      else {
        a_request.setStatus(GlobalVariables.Suspended);
        ResourceCB.rrb_array_list_.add(a_request);
        a_thread.suspend(a_request);
      } //end else
      break;
    case GlobalVariables.Detection:
      if (quantity <= this.getAvailable()) {
        a_request.setStatus(GlobalVariables.Granted);
        a_request.grant();
      } //end if
      else if (this.getTotal() < quantity) {
        return null;
      } //end else if
      else {
        a_request.setStatus(GlobalVariables.Suspended);
        ResourceCB.rrb_array_list_.add(a_request);
        a_thread.suspend(a_request);
      } //end else
      break;
    default:
      System.out.println("Error in Swtich Case");
      break;
    }//end switch
    return a_request;
  }//end do_acquire

  /****************************************************************
   * Function 'do_deadlockDetection'
  **/
  public static Vector<ThreadCB> do_deadlockDetection() {
    int rrb_count = ResourceCB.rrb_array_list_.size();
    int[] working_array = new int[ResourceTable.getSize()];
    boolean[] finished_array = new boolean[ResourceCB.thread_array_list_.size()];
    int[][] request_array = new int[ResourceCB.thread_array_list_.size()][ResourceTable.getSize()];
    for (int i = 0; i < rrb_count; i++) {
      int thread_int = -1;
      int type = -1;
      RRB a_request = ResourceCB.rrb_array_list_.get(i);
      ThreadCB a_thread = a_request.getThread();
      request_array[thread_int][type] = a_request.getQuantity();
      for (int j = 0; j < ResourceCB.thread_array_list_.size(); j++) {
        if (ResourceCB.thread_array_list_.get(j) == a_thread) {
          thread_int = j;
        } //end if
      } //end for j
      for (int j = 0; j < size_; j++) {
        if (ResourceCB.rrb_array_list_.get(i).getResource() == ResourceTable.getResourceCB(j)) {
          type = j;
        } //end if
      } //end for j
    } //end for i
    for (int i = 0; i < size_; i++) {
      working_array[i] = ResourceTable.getResourceCB(i).getAvailable();
    } //end for i
    for (int i = 0; i < size_; i++) {
      finished_array[i] = true;
      for (int j = 0; j < ResourceTable.getSize(); j++) {
        if (0 < (ResourceTable.getResourceCB(j).getAllocated(ResourceCB.thread_array_list_.get(i)))) {
          finished_array[i] = false;
        } //end if
      } //end for j
    } //end for i
    boolean this_index = false;
    boolean first_run = true;
    while (first_run || this_index) {
      this_index = false;
      first_run = false;
      for (int i = 0; i < finished_array.length; i++) {
        if (finished_array[i] == false) {
          boolean request_i_less_work = true;
          for (int j = 0; j < rrb_count; j++) {
            if (request_array[i][j] > working_array[j])
              request_i_less_work = false;
          } //end for j
          if (request_i_less_work == true) {
            this_index = true;
            for (int k = 0; k < working_array.length; k++) {
              working_array[k] += ResourceTable.getResourceCB(k).getAllocated(ResourceCB.thread_array_list_.get(i));
              finished_array[i] = true;
            } //end for k
            break;
          } //end if
        } //end for if
      } //end for i
    } //end wile
    Vector<ThreadCB> thread_vector = new Vector<ThreadCB>();
    if (this_index == false) {
      for (int i = 0; i < finished_array.length; i++) {
        if (finished_array[i] == false) {
          thread_vector.add(ResourceCB.thread_array_list_.get(i));
        } //end if
      } //end for i
    } //end if
    if (thread_vector.size() == 0) {
      return null;
    } //end if
    Vector<ThreadCB> thread_vector_2 = thread_vector;
    while (thread_vector_2 != null) {
      ThreadCB a_thread = thread_vector_2.get(0);
      a_thread.kill();
      thread_vector_2 = ResourceCB.do_deadlockDetection();
    } //end while
    ResourceCB.is_granted();
    return thread_vector;
  }//end do_deadlockDetection

  /****************************************************************
   * Function 'do_giveupResources'
  **/
  public static void do_giveupResources(ThreadCB thread) {
    for (int i = 0; i < size_; i++) {
      ResourceCB a_resource = ResourceTable.getResourceCB(i);
      a_resource.setAvailable(a_resource.getAvailable() + a_resource.getAllocated(thread));
      a_resource.setAllocated(thread, 0);
    } //end for i
    for (int i = 0; i < ResourceCB.rrb_array_list_.size(); i++) {
      if (ResourceCB.rrb_array_list_.get(i).getThread() == thread) {
        ResourceCB.rrb_array_list_.remove(i);
      } //end if
    } //enf for i
    ResourceCB.is_granted();
  }//end do_giveupResources

  /****************************************************************
   *
  **/
  public void do_release(int quantity) {
    ThreadCB a_thread = null;
    if (0 <= quantity) {
      try {
        a_thread = MMU.getPTBR().getTask().getCurrentThread();
      } //end try
      catch (NullPointerException exception) {
        System.out.println("Null Pointer Exception" + exception.getMessage());
      } //end catch
      this.setAvailable(this.getAvailable() + quantity);
      this.setAllocated(a_thread, this.getAllocated(a_thread) - quantity);
      ResourceCB.is_granted();
    } //end if
  }//end do_release

  /****************************************************************
   *
  **/
  public static void atError() {
    System.out.println("Error");
  }//end atError

  /****************************************************************
   *
  **/
  public static void atWarning() {
    System.out.println("Warning");
  }//end atWarning

  /****************************************************************
   *
  **/
  private static boolean bankers_algorithm() {
    int num_threads = ResourceCB.thread_array_list_.size();
    int[] working_array = new int[ResourceTable.getSize()];
    boolean[] finished_array = new boolean[num_threads];
    int[][] Max = new int[num_threads][ResourceTable.getSize()];
    int[][] Request = new int[num_threads][ResourceTable.getSize()];
    for (int i = 0; i < num_threads; i++) {
      for (int j = 0; j < ResourceTable.getSize(); j++) {
        Max[i][j] = ResourceTable.getResourceCB(j).getMaxClaim(ResourceCB.thread_array_list_.get(i));
      } //end for j
    } //end for i
    for (int i = 0; i < ResourceCB.rrb_array_list_.size(); i++) {
      int thread_int = -1;
      int type = -1;
      RRB a_request = ResourceCB.rrb_array_list_.get(i);
      ThreadCB a_thread = a_request.getThread();
      for (int j = 0; j < ResourceCB.thread_array_list_.size(); j++) {
        if (ResourceCB.thread_array_list_.get(j) == a_thread) {
          thread_int = j;
        } //end if
      } //end for j
      for (int j = 0; j < size_; j++) {
        if (ResourceCB.rrb_array_list_.get(i).getResource() == ResourceTable.getResourceCB(j)) {
          type = j;
        } //end if
      } //end for j
      Request[thread_int][type] = a_request.getQuantity();
    } //end for i
    for (int i = 0; i < size_; i++) {
      working_array[i] = ResourceTable.getResourceCB(i).getAvailable();
    } //end for i
    for (int i = 0; i < num_threads; i++) {
      finished_array[i] = false;
    } //end for i
    boolean this_element = true;
    while (this_element) {
      this_element = false;
      for (int i = 0; i < num_threads; i++) {
        if (finished_array[i] == false) {
          boolean to_remove = true;
          for (int j = 0; j < size_; j++) {
            int needed = Max[i][j] - ResourceTable.getResourceCB(j).getAllocated(ResourceCB.thread_array_list_.get(i));
            if (needed > working_array[j]) {
              to_remove = false;
            } //end if
          } //end for j
          if (to_remove) {
            this_element = true;
            for (int j = 0; j < size_; j++) {
              working_array[j] += ResourceTable.getResourceCB(j).getAllocated(ResourceCB.thread_array_list_.get(i));
            } //end for j
            finished_array[i] = true;
            break;
          } //end if
        } //end if
      } //end for
    } //end while
    for (int i = 0; i < num_threads; i++) {
      if (finished_array[i] == false) {
        return false;
      } //end if
    } //end for i
    return true;
  }//end bankers_algorithm

  /****************************************************************
   * Function 'is_granted'
  **/
  private static void is_granted() {
    int rrb_count = ResourceCB.rrb_array_list_.size();
    for (int i = 0; i < rrb_count; i++) {
      int type = -1;
      RRB a_request = ResourceCB.rrb_array_list_.get(i);
      ResourceCB a_resource = ResourceTable.getResourceCB(type);
      for (int j = 0; j < size_; j++) {
        if (ResourceCB.rrb_array_list_.get(i).getResource() == ResourceTable.getResourceCB(j)) {
          type = j;
        } //end if
      } //end for j
      if ((a_request.getQuantity() <= a_resource.getAvailable()) && (a_request.getQuantity() >= 0)) {
        a_resource.setAvailable(a_resource.getAvailable() - a_request.getQuantity());
        a_resource.setAllocated(a_request.getThread(),
            a_resource.getAllocated(a_request.getThread()) + a_request.getQuantity());
        a_request.setStatus(GlobalVariables.Granted);
        a_request.grant();
        ResourceCB.rrb_array_list_.remove(i);
      } //end if
    } //end for i
  }//end is_granted

}//end ResourceCB

/****************************************************************
 * End 'ResourceCB.java'
**/
